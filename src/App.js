import logo from './logo.svg';
import './App.css';
import Header from './components/Header/Header.js';
import BasicButton from './components/BasicButton/BasicButton.js';
import StarterButton from './components/StarterButton/StarterButton.js';
import ChildButton from './components/ChildButton/ChildButton.js';
import Cat from './components/Cat/Cat.js';

import { useState } from 'react';

    

function App() {
  const [colour, setColour] = useState('white');
  const [catCounter, setCatCounter] = useState(0);

  function updateColour() {
    if (colour === 'white') {
        setColour('black');
    } else {
        setColour('white');
    }
}

  function getNewCat() {
    setCatCounter(catCounter + 1);
  }

  return (
    <div className="App" style={{backgroundColor: colour}}>
      <Header/>
      <BasicButton/>
      <StarterButton colourOne="lime" colourTwo="magenta"/>
      <ChildButton changeColour={updateColour} getNewCat={getNewCat}/>
      <Cat propToForceRerender={catCounter}/>
      
    </div>
  );
}

export default App;
