//This component is a div containing a button. When you click on the button, it 
// tells the parent component to change its colour.
import './ChildButton.css';

function ChildButton(props) {

    function handleClick(e) {
        props.changeColour();
        props.getNewCat();
    }

    return (
        <div className="SimpleButton">
            <button onClick={handleClick}>Change my parent!</button>
        </div>
    );
}

export default ChildButton;