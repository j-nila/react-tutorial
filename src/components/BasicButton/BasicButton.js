//This component is a div containing a button. When you click on the button, the 
// the background color of the div changes, thn alternates between red and blue.
import './BasicButton.css';
import { useState } from 'react';

function BasicButton() {
    const [colour, setColour] = useState('red');

    function changeColour() {
        if (colour === 'red') {
            setColour('blue');
        } else {
            setColour('red');
        }
    }

    return (
        <div className="SimpleButton" style={{ backgroundColor: colour }}>
            <button onClick={changeColour}>Change the colour!</button>
        </div>
    );
}

export default BasicButton;