//gets the image if a random cat from https://cataas.com/cat?json=true
//and displays it

import { useState, useEffect } from "react";

function Cat(props) {
  const [cat, setCat] = useState(null);

  useEffect(() => {
    fetch("https://cataas.com/cat?json=true")
      .then((res) => res.json())
      .then((data) => setCat(data.url));
  }, [props.propToForceRerender]);

  return (
    <div>
      <h1>Random Cat</h1>
      {/*only display the image if cat is not null*/}
      {cat && <img src={`https://cataas.com${cat}`} alt="cat" />}
    </div>
  );
}

export default Cat;