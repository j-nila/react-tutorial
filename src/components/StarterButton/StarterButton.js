//This component is a div containing a button. When you click on the button, the 
// the background color of the div changes, thn alternates between props one and two.
import './StarterButton.css';
import { useState } from 'react';

function StarterButton(props) {
    const [colour, setColour] = useState(props.colourOne);

    function changeColour() {
        if (colour === props.colourOne) {
            setColour(props.colourTwo);
        } else {
            setColour(props.colourOne);
        }
    }

    return (
        <div className="StarterButton" style={{ backgroundColor: colour }}>
            <button onClick={changeColour}>Change the colour!</button>
        </div>
    );
}

export default StarterButton;